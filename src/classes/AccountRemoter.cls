global with sharing class AccountRemoter {

    public String accountName { get; set; }
    public static Account account { get; set; }
    public AccountRemoter() { } // empty constructor
    
    @RemoteAction  @AuraEnabled
    public static Object getAccount() {
        system.debug('@@ Get Account Function :');
          return tempUtilityClass.contCall();
        //return account;
    }


    public static Object addressCallBackHandler(List<String> labels, Object state)
    {

    	 system.debug('@@@ Address Call Back Handler .. Start');
         if(labels == null)
          system.debug('No Continuation');

          List<DataSanAddressResult> addList = new List<DataSanAddressResult>();

            for(String str : labels)
            {
                HttpResponse resp =  Continuation.getResponse(str);
                DataSanAddressResult oresult = (DataSanAddressResult)JSON.deserialize(resp.getBody(),DataSanAddressResult.class);
                addList.add(oresult);
            }
            //EtdBHandler.addressList =addList;
        
            return  addList;
    }
}