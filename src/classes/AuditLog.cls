public class AuditLog {
	public AuditLog() {
		
	}

	public static boolean showDebugWithDateTime = false;


/*
Write anything into debug console with single line function.
Marking top and bottom of the string 
*/

    
	public static void debug(String writeToDebug)
	{
        String topStr = ' ** - Start - **';
		String bottomStr = ' ** - End - **';
		String writeStr = topStr;
		if(showDebugWithDateTime)
		{
			writeStr =writeStr+'\n'+String.valueOf(Datetime.now()); 
        }
        writeStr= writeStr+ '\n'+writeToDebug+'\n'+bottomStr;
		system.debug(writeStr);
	}

	public static void debug(Exception ex)
	{
		
	}

	
}