public with sharing class ContactController {

    @AuraEnabled
    public static List<Contact> findAll() {
        return [SELECT id, name, phone FROM Contact LIMIT 10];
    }

    @AuraEnabled
    public static List<Contact> findByName(String searchKey) {
        String name = '%' + searchKey + '%';
        return [SELECT id, name, phone FROM Contact WHERE name LIKE :name LIMIT 50];
    }

    @AuraEnabled
    public static Contact findById(String contactId) {
        return [SELECT id, name, title, phone, mobilephone, Account.Name
                    FROM Contact WHERE Id = :contactId];
    }

 private static final String LONG_RUNNING_SERVICE_URL = 'https://rapidmatch.datasan.com.au/dpid_lookup?auth_token=O0EwFQZVMYYOmbB4TIz-FrJMXHqviMVLAQRQRj2LTuEDvB0miAEC3sJ_1o1BNGoB&dpid=85877543';


    @RemoteAction
    public static Object continuationRemote() 
    {
    	  
          system.debug('++ REMOTE CONTINUATION CALL  ++');
        
              return tempUtilityClass.contCall();
    }

 @AuraEnabled
    public static Object continuationUsingAura() 
    {
         system.debug('++ AURA CONTINUATION CALL  ++');
        Object r = continuationRemote();
              return tempUtilityClass.contCall();
    }


  

    public static Object addressCallBackHandler(List<String> labels, Object state)
    {

    	 system.debug('@@@ Address Call Back Handler .. Start');
    	 if(labels == null)
    	  system.debug('No Continuation');

    	List<Object> addList = new List<Object>();

			for(String str : labels)
			{
				HttpResponse resp =  Continuation.getResponse(str);
				addList.add(resp.getBody());
			}
          
          system.debug('++ THUNDER STRUCK :::: '+addList);
        
        	return addList;
    }

}