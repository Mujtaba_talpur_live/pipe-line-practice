public class DataSanAddressResult {

	public Response response;

	public class Response {
		public Integer numFound;
		public Integer start;
		public List<Docs> docs;
	}

	public class Docs {
		public String address;
		public String address_line_1;
		public String delivery_point_id;
		public Boolean is_street_address_type;
		public Boolean is_postal_delivery_address_type;
		public Boolean is_lot_nbr_address_type;
		public String flat_unit_type;
		public String flat_unit_type_abbr;
		public String flat_unit_nbr;
		public String floor_level_type;
		public String floor_level_type_abbr;
		public String floor_level_nbr;
		public String building_property_name_1 {get {return '';} set;} //Setting the value to blank as resolution for HP QC Defect #993
		public String building_property_name_2 {get {return '';} set;} //Setting the value to blank as resolution for HP QC Defect #993
		public String lot_nbr;
		public String postal_delivery_type;
		public String postal_delivery_type_abbr;
		public String postal_delivery_nbr_pfx;
		public String postal_delivery_nbr;
		public String postal_delivery_nbr_sfx;
		public String house_nbr_1;
		public String house_nbr_sfx_1;
		public String house_nbr_2;
		public String house_nbr_sfx_2;
		public String street_name;
		public String street_type;
		public String street_type_abbr;
		public String street_sfx;
		public String street_sfx_abbr;
		public String locality_name;
		public String state;
		public String postcode;		
	}
	
	public static DataSanAddressResult parse(String json) {
		return (DataSanAddressResult) System.JSON.deserialize(json, DataSanAddressResult.class);
	}
}