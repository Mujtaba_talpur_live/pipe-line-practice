public abstract class Food {
	
	private String prvfoodName;
	public String foodName {get{return prvfoodName;}}
	public boolean isVegetable{get;set;}
	public boolean isFruit{get;set;}
	public boolean isFastFood{get;set;}
	public boolean isHomeCooked{get;set;}
	public boolean containsVegetable{get;set;}
	public boolean containsMeat{get;set;}
	public enum MeatType {chicken,mutton,beef}
	public virtual void myFoodName(String nameOfFood)
	{
		prvfoodName = nameOfFood;
	}
	
	public void printFoodName()
	{
		if(foodName == null || foodName == '')
		 system.debug('No Food Name set Yet');
		else
		 system.debug(foodName);
	}
	
	public void songClassTesting()
	{
	   Song.englishSong eSong = new Song.englishSong();
	   eSong.name = 'Adele';
	   eSong.artist = 'Rolling in Deep';
	   system.debug('English Song :'+eSong);
	}
	
	
    
}