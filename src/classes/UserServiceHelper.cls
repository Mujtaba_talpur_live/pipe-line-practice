public without sharing class UserServiceHelper {  // Description: generate a GUID
    public static String NewGuid() {
        String kHexChars = '0123456789abcdef';
        String returnValue = '';
        Integer nextByte = 0;
        for (Integer i=0; i<16; i++) {
            if (i==4 || i==6 || i==8 || i==10) 
                returnValue += '-';
                nextByte = (Math.round(Math.random() * 255)-128) & 255;
            if (i==6) {
                nextByte = nextByte & 15;
                nextByte = nextByte | (4 << 4);
            }
            if (i==8) {
                nextByte = nextByte & 63;
                nextByte = nextByte | 128;
            }
            returnValue += charAtIndex(kHexChars, nextByte >> 4);
            returnValue += charAtIndex(kHexChars, nextByte & 15);
        }

        return returnValue;
    }

      private static String charAtIndex(String str, Integer index) {
        if (str == null) return null;
        if (str.length() <= 0) return str;    
        if (index == str.length()) return null;    
        return str.substring(index, index+1);
    }
}