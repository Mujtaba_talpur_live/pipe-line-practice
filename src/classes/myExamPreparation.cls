global class myExamPreparation {
    public myExamPreparation() {
        
    }
    
    @InvocableVariable
    public string iamInvocableVariable;


    transient integer age = 20;

    @InvocableMethod
    public static List<Vehicle__c> retrieveAllVehicles(List<Vehicle__c> anything)
    {
        return [Select id,name from Vehicle__c];
    }
}