@restresource(urlMapping ='/restclassexp/*')
global class restClassExp {
	
	@HttpGet
	global static Vehicle__c doGet()
	{
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		String vid = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
		//String vid = String.valueOf(req.params['ID']);
		Vehicle__c result = [Select v.Name, v.Id, v.CreatedDate From Vehicle__c v WHERE v.id = :vid limit 1];
		return result;
	}
     
    
    
}