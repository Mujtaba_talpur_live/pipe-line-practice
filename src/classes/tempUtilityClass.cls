public with sharing class tempUtilityClass {
	public static Object ContinuationGenerator(List<HttpRequest> reqs,String methodName,String stateJson)
    {
         system.debug('ContinuationGenerator(List<HttpRequest> reqs,String methodName,String stateJson)');
        Continuation con = new Continuation(120);
        try
        {
            for(HttpRequest request : reqs)
            {
                String testtEMP = con.addHttpRequest(request);
                system.debug('@@@@ testtEMP  ::'+testtEMP );
            }
            con.continuationMethod =methodName;
            con.state=stateJson;
              system.debug(stateJson);
               system.debug(con);
        }
        catch(Exception ex)
        {
            System.debug('ContinuationGenerator(List<HttpRequest> reqs,String methodName,String stateJson) function in ETDBHanlder failed ');   
        }
        return con;
    }
 private static final String LONG_RUNNING_SERVICE_URL = 'https://rapidmatch.datasan.com.au/dpid_lookup?auth_token=O0EwFQZVMYYOmbB4TIz-FrJMXHqviMVLAQRQRj2LTuEDvB0miAEC3sJ_1o1BNGoB&dpid=85877543';

    public static Object contCall()
    {
    	  List<httpRequest> allreqs = new List<httpRequest>();

                    String endPoint = LONG_RUNNING_SERVICE_URL;
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(endPoint);
                    req.setMethod('GET');
                    allreqs.add(req);
                    string guiString= 'Goofy text'; 
                   // system.debug('I am in the continuation call section :::: '+ allreqs);


              return tempUtilityClass.ContinuationGenerator(allreqs,'addressCallBackHandler',guiString);
    }

}